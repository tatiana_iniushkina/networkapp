package ru.demo.url;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Демонстрация получения url байт-кода текущего класса
 * и вывода в 16-тиричном виде первых 128 байт
 */
public class UrlDemo {
    public static void main(String[] args) throws MalformedURLException {
        URL url = UrlDemo.class.getResource("UrlDemo.class");
//        URL url = new URL("http://google.ru");
        System.out.println(url);
        try (InputStream inputStream = url.openStream()){
            byte buffer[] = new byte[128];
            int buffersRead = inputStream.read(buffer);
            for (int i = 0; i < buffersRead; i++) {
                if (i % 32 == 0) {
                    System.out.println();
                }
                System.out.printf("%02X ", buffer[i]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
