package ru.demo.socket.datagram;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

/**
 * Работа по протоколу UDP.
 * Клиент кодирует строку  и отправляет
 * на localhost на 11111 порт
 */
public class DatagramClient {
    public static void main(String[] args) throws IOException {
        try (DatagramSocket socket = new DatagramSocket()) {
            DatagramPacket packet = encodePacket("Hello world!");
            socket.send(packet);
            socket.close();
        }
    }

    private static DatagramPacket encodePacket(String text) throws UnknownHostException {
        byte bytes[] = text.getBytes(StandardCharsets.UTF_8);
        InetAddress inetAddress = InetAddress.getLocalHost();
        return new DatagramPacket(bytes, bytes.length, inetAddress, 11111);
    }
}
